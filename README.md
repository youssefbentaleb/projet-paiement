## Quick Start

1.  Clone the repo `git clone https://gitlab.com/youssefbentaleb/projet-paiement.git`
2.  Go to your project folder from your terminal
3.  Run: `docker-compose up` (ports used are: 9001, 8080, 3000)
4.  After open the browser and go to: (http://localhost:3000)
5.  SignUp and login with your account


## Repo for the projects

1. Backend:  (https://gitlab.com/youssefbentaleb/back-paiement.git)
2. Frontend: (https://gitlab.com/youssefbentaleb/front-paiement-velocity.git)